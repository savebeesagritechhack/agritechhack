﻿using Microsoft.EntityFrameworkCore;
using SaveBees.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SaveBees.Services
{
    public interface ISignalService
    {
        Task<List<Signal>> GetAllAsync();
        Task<Signal> GetByIdAsync(int id);
    }

    public class SignalService : ISignalService
    {
        private readonly SaveBeesContext _context;

        public SignalService(SaveBeesContext context)
        {
            _context = context;
        }

        public async Task<List<Signal>> GetAllAsync()
        {
            return await _context.Signals
                .Include(e => e.User)
                .ToListAsync();
        }

        public async Task<Signal> GetByIdAsync(int id)
        {
            return await _context.Signals
                .Include(e => e.User)
                .SingleOrDefaultAsync(e => e.Id == id);
        }
    }
}
