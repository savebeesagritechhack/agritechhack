﻿using Microsoft.EntityFrameworkCore;
using SaveBees.Models;
using SaveBees.Security;
using SaveBees.Services.Communication;
using System.Threading.Tasks;

namespace SaveBees.Services
{
    public interface IAuthService
    {
        Task<TokenResponse> CreateAccessTokenAsync(string email, string password);
        Task<TokenResponse> RefreshTokenAsync(string refreshToken, string email);
        void RevokeRefreshToken(string refreshToken);
    }

    public class AuthService : IAuthService
    {
        private readonly SaveBeesContext _context;
        private readonly ITokenHandler _tokenHandler;
        private readonly IPasswordHasher _passwordHasher;

        public AuthService(SaveBeesContext context, ITokenHandler tokenHandler, IPasswordHasher passwordHasher)
        {
            _context = context;
            _tokenHandler = tokenHandler;
            _passwordHasher = passwordHasher;
        }

        public async Task<TokenResponse> CreateAccessTokenAsync(string email, string password)
        {
            var user = await _context.Users
                .Include(e => e.UserRoles)
                    .ThenInclude(e => e.Role)
                .SingleOrDefaultAsync(e => e.Email == email);

            if (user == null || !_passwordHasher.PasswordMatches(password, user.Password))
            {
                return new TokenResponse(false, "Invalid credentials.", null);
            }

            var accessToken = _tokenHandler.CreateAccessToken(user);

            return new TokenResponse(true, null, accessToken);
        }

        /// <summary>
        /// A better way would be to store it in the database.
        /// </summary>
        /// <returns></returns>
        public async Task<TokenResponse> RefreshTokenAsync(string refreshToken, string email)
        {
            var token = _tokenHandler.TakeRefreshToken(refreshToken);

            if (token == null)
            {
                return new TokenResponse(false, "Cannot find that refresh token.", null);
            }

            if (token.IsExpired())
            {
                return new TokenResponse(false, "Expired refresh token.", null);
            }

            var user = await _context.Users
                .Include(e => e.UserRoles)
                    .ThenInclude(e => e.Role)
                .SingleOrDefaultAsync(e => e.Email == email);

            if (user == null)
            {
                return new TokenResponse(false, "Invalid refresh token.", null);
            }

            var accessToken = _tokenHandler.CreateAccessToken(user);

            return new TokenResponse(true, null, accessToken);
        }

        public void RevokeRefreshToken(string refreshToken)
        {
            _tokenHandler.RevokeRefreshToken(refreshToken);
        }
    }
}
