﻿using Microsoft.EntityFrameworkCore;
using SaveBees.Models;
using SaveBees.Security;
using SaveBees.Services.Communication;
using System.Threading.Tasks;

namespace SaveBees.Services
{
    public interface IUserService
    {
        Task<CreateUserResponse> CreateUserAsync(User user, string role);
    }

    public class UserService : IUserService
    {
        private readonly SaveBeesContext _context;
        private readonly IPasswordHasher _passwordHasher;

        public UserService(SaveBeesContext context, IPasswordHasher passwordHasher)
        {
            _context = context;
            _passwordHasher = passwordHasher;
        }

        public async Task<CreateUserResponse> CreateUserAsync(User user, string role)
        {
            var existingUser = await _context.Users
                .Include(e => e.UserRoles)
                    .ThenInclude(e => e.Role)
                .SingleOrDefaultAsync(e => e.Email == user.Email);

            if (existingUser != null)
            {
                return new CreateUserResponse(false, "E-mail already in use.", null);
            }

            user.Password = _passwordHasher.HashPassword(user.Password);

            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
            await AddUserInRoleAsync(user.Id, role);

            return new CreateUserResponse(true, null, user);
        }

        private async Task<bool> AddUserInRoleAsync(int id, string roleName)
        {
            var user = await _context.Users
               .Include(e => e.UserRoles)
                   .ThenInclude(e => e.Role)
               .SingleOrDefaultAsync(e => e.Id == id);

            if (user == null)
            {
                return false;
            }

            var role = await _context.Roles
               .Include(e => e.UserRoles)
                   .ThenInclude(e => e.User)
               .SingleOrDefaultAsync(e => e.Name == roleName);

            if (role == null)
            {
                return false;
            }

            user.UserRoles.Add(new UserRole { RoleId = role.Id });
            _context.Users.Update(user);
            var updated = await _context.SaveChangesAsync();
            return updated > 0;
        }
    }
}
