﻿using AutoMapper;
using SaveBees.Dtos;
using SaveBees.Models;
using SaveBees.Security;
using System.Linq;

namespace Binance.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<AccessToken, AccessTokenDto>()
                .ForMember(e => e.AccessToken, opt => opt.MapFrom(e => e.Token))
                .ForMember(e => e.RefreshToken, opt => opt.MapFrom(e => e.RefreshToken.Token))
                .ForMember(e => e.Expiration, opt => opt.MapFrom(e => e.Expiration));

            CreateMap<User, UserProfileDto>()
               .ForMember(e => e.Roles, opt => opt.MapFrom(e => e.UserRoles.Select(ur => ur.Role.Name)));
        }
    }
}
