﻿namespace SaveBees.Dtos
{
    public class RefreshTokenRevokeDto
    {
        public string RefreshToken { get; set; }
    }
}
