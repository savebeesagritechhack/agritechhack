﻿using Microsoft.AspNetCore.Mvc;
using SaveBees.Services;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using SaveBees.Security;
using SaveBees.Dtos;
using AutoMapper;

namespace SaveBees.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly IMapper _mapper;

        public AuthController(IAuthService authService, IMapper mapper)
        {
            _authService = authService;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [Route("login")]
        [HttpPost]
        public async Task<ActionResult<AccessToken>> LoginAsync([FromBody] UserLoginDto userLoginDto)
        {
            var response = await _authService.CreateAccessTokenAsync(userLoginDto.Email, userLoginDto.Password);

            if (!response.Success)
            {
                return BadRequest(response.Message);
            }

            var accessTokenDto = _mapper.Map<AccessToken, AccessTokenDto>(response.Token);
            return Ok(accessTokenDto);
        }

        [AllowAnonymous]
        [Route("token/refresh")]
        [HttpPost]
        public async Task<ActionResult<AccessToken>> RefreshTokenAsync([FromBody] RefreshTokenDto refreshTokenDto)
        {
            var response = await _authService.RefreshTokenAsync(refreshTokenDto.RefreshToken, refreshTokenDto.Email);

            if (!response.Success)
            {
                return BadRequest(response.Message);
            }

            var accessTokenDto = _mapper.Map<AccessToken, AccessTokenDto>(response.Token);
            return Ok(accessTokenDto);
        }

        [AllowAnonymous]
        [Route("token/revoke")]
        [HttpPost]
        public ActionResult RevokeToken([FromBody] RefreshTokenRevokeDto refreshTokenRevokeDto)
        {
            _authService.RevokeRefreshToken(refreshTokenRevokeDto.RefreshToken);
            return NoContent();
        }
    }
}
