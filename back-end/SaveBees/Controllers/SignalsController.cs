﻿using Microsoft.AspNetCore.Mvc;
using SaveBees.Models;
using SaveBees.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SaveBees.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SignalsController : ControllerBase
    {
        private readonly ISignalService _signalService;
        
        public SignalsController(ISignalService signalService)
        {
            _signalService = signalService;
        }

        // GET: api/Signals
        [HttpGet]
        public async Task<ActionResult<List<Signal>>> GetAllAsync()
        {
            var signals = await _signalService.GetAllAsync();
            return Ok(signals);
        }

        // GET: api/Signals/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Signal>> GetByIdAsync([FromRoute] int id)
        {
            var signal = await _signalService.GetByIdAsync(id);

            if (signal == null)
            {
                return NotFound();
            }

            return Ok(signal);
        }
    }
}
