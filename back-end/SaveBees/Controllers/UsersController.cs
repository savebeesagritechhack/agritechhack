﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SaveBees.Dtos;
using SaveBees.Models;
using SaveBees.Services;
using System.Threading.Tasks;

namespace SaveBees.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UsersController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> CreateUserAsync([FromBody] UserRegisterDto userRegisterDto)
        {
            var user = new User
            {
                FirstName = userRegisterDto.FirstName,
                MiddleName = userRegisterDto.MiddleName,
                LastName = userRegisterDto.LastName,
                Email = userRegisterDto.Email,
                Password = userRegisterDto.Password,
                Phone = userRegisterDto.Phone
            };

            var response = await _userService.CreateUserAsync(user, userRegisterDto.Role);

            if (!response.Success)
            {
                return BadRequest(response.Message);
            }

            return Ok("Successfully registered.");
        }
    }
}
