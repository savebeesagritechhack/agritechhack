﻿using System;
using System.Collections.Generic;

namespace SaveBees.Models
{
    public class Signal
    {
        public int Id { get; set; }
        public List<string> Coordinates { get; set; }
        public DateTime Date { get; set; }
        public string Sprayer { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}
