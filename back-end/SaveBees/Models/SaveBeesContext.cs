﻿using GeoAPI.Geometries;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite;
using SaveBees.Security;
using System;
using System.Collections.Generic;

namespace SaveBees.Models
{
    public class SaveBeesContext : DbContext
    {
        private readonly IPasswordHasher _passwordHasher;

        public SaveBeesContext(DbContextOptions<SaveBeesContext> options, IPasswordHasher passwordHasher)
            : base(options)
        {
            _passwordHasher = passwordHasher;
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Signal> Signals { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<User>(entity =>
            {
                // Required & Max length
                entity.Property(u => u.FirstName)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(u => u.MiddleName)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(u => u.LastName)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(u => u.Email)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(u => u.Password)
                    .IsRequired();

                // Unique key
                entity.HasIndex(u => u.Email)
                  .IsUnique();
            });

            builder.Entity<Role>(entity =>
            {
                // Required & Max length
                entity.Property(r => r.Name)
                  .IsRequired()
                  .HasMaxLength(20);

                // Unique key
                entity.HasIndex(r => r.Name)
                  .IsUnique();
            });

            builder.Entity<Signal>(entity =>
            {
                // Required
                entity.Property(r => r.Coordinates)
                  .IsRequired();

                entity.Property(r => r.Sprayer)
                  .IsRequired();
            });

            // Many-to-many
            builder.Entity<UserRole>()
                .HasKey(ur => new { ur.UserId, ur.RoleId });

            builder.Entity<UserRole>()
                .HasOne(ur => ur.User)
                .WithMany(u => u.UserRoles)
                .HasForeignKey(ur => ur.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<UserRole>()
                .HasOne(ur => ur.Role)
                .WithMany(r => r.UserRoles)
                .HasForeignKey(ur => ur.RoleId)
                .OnDelete(DeleteBehavior.Cascade);

            // One-to-many
            builder.Entity<Signal>()
               .HasOne(s => s.User)
               .WithMany(u => u.Signals)
               .HasForeignKey(s => s.UserId)
               .OnDelete(DeleteBehavior.Cascade);

            // Seed database
            builder.Entity<User>().HasData(
                new { Id = 1, FirstName = "Admin", MiddleName = "Admin", LastName = "Admin", Email = "test@gmail.com", Password = _passwordHasher.HashPassword("123456") });

            builder.Entity<Role>().HasData(
                new { Id = 1, Name = "Beekeeper" },
                new { Id = 2, Name = "Farmer" },
                new { Id = 3, Name = "Administrator" });

            builder.Entity<UserRole>().HasData(
                new { UserId = 1, RoleId = 3 });

            builder.Entity<Signal>().HasData(
                new
                {
                    Id = 1,
                    Coordinates = new List<List<List<double>>>
                    {
                        new List<List<double>>()
                    },
                    Date = DateTime.Now.AddDays(7),
                    Sprayer = "Pesticides",
                    UserId = 1
                }
            );
        }
    }
}
