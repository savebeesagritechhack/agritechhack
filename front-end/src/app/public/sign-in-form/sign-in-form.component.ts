import { Component, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-sign-in-form',
  templateUrl: './sign-in-form.component.html',
  styleUrls: ['./sign-in-form.component.css']
})
export class SignInFormComponent implements OnDestroy {
  form: FormGroup;

  private componentDestroyed$ = new Subject<boolean>();

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute) {
    this.form = this.initializeForm();
  }

  ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  private initializeForm() {
    return this.formBuilder.group({
      email: [null, [Validators.required, Validators.email, Validators.maxLength(20)]],
      password: [null, [Validators.required, Validators.minLength(6)]]
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    this.authService.login(this.form.value)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(success => {
        if (success) {
          const returnUrl = this.activatedRoute.snapshot.queryParams.returnUrl;
          
          if (returnUrl) {
            return this.router.navigate([decodeURIComponent(returnUrl)]);
          } else {
            return this.router.navigate(['/']);
          }
        }
      });
  }
}
