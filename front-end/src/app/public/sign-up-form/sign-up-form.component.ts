import { Component, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MustMatch } from 'src/app/shared/must-match';
import { BsModalService } from 'ngx-bootstrap';

import { InformationModalComponent } from 'src/app/shared/information-modal/information-modal.component';

import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-sign-up-form',
  templateUrl: './sign-up-form.component.html',
  styleUrls: ['./sign-up-form.component.css']
})
export class SignUpFormComponent implements OnDestroy {
  form: FormGroup;

  private componentDestroyed$ = new Subject<boolean>();

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService) {
    this.form = this.initializeForm();
  }

  ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  private initializeForm() {
    return this.formBuilder.group({
      firstName: [null, [Validators.required, Validators.maxLength(20)]],
      middleName: [null, [Validators.required, Validators.maxLength(20)]],
      lastName: [null, [Validators.required, Validators.maxLength(20)]],
      email: [null, [Validators.required, Validators.email, Validators.maxLength(20)]],
      password: [null, [Validators.required, Validators.minLength(6)]],
      confirmPassword: [null, [Validators.required]],
      phone: [null],
      role: [null, [Validators.required]]
    }, { validator: MustMatch('password', 'confirmPassword') });
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    this.userService.register(this.form.value)
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(success => {
        const initialState = {
          title: 'Information',
          message: success
        };

        this.modalService.show(InformationModalComponent, { initialState });
      });
  }
}
