import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './log-in.component';

import { NoSessionGuard } from '../core/guards/no-session.guard';

const routes: Routes = [
  {
    path: 'log-in',
    component: LoginComponent,
    canActivate: [NoSessionGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule {}
