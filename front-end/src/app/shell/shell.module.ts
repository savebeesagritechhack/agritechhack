import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { CollapseModule } from 'ngx-bootstrap';

import { ShellComponent } from './shell.component';

@NgModule({
  declarations: [ShellComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    CollapseModule.forRoot()
  ],
  exports: [ShellComponent]
})
export class ShellModule { }