import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.css']
})
export class ShellComponent {
  isNavbarCollapsed = true;
  
  @Input() isLoggedIn: boolean;
  @Output() logoutClicked = new EventEmitter();

  onLogoutClicked() {
    this.logoutClicked.emit();
  }
}
