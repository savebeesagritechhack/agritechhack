import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { ValuesService } from 'src/app/core/services/values.services';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent {
  values$: Observable<string[]>;

  constructor(private valuesService: ValuesService) {
    this.values$ = this.valuesService.getAllValues();
  }
}
