import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as MapboxDraw from '@mapbox/mapbox-gl-draw';
import * as turf from '@turf/turf';
import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  map: mapboxgl.Map;

  ngOnInit() {
    Object.getOwnPropertyDescriptor(mapboxgl, 'accessToken').set(environment.mapbox.accessToken);

    this.map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/satellite-streets-v10',
      zoom: 6,
      center: [25.4833039, 42.7249925],
      attributionControl: false
    });

    //===========================================
    // Get mouse coordinates (longtitute/latitute)
    //===========================================
    // this.map.on('mousemove', e => {
    //   console.log(e.lngLat.wrap());
    // });
    //===========================================

    //===========================================
    // Marker for Beekeepers
    //===========================================
    // const popup = new mapboxgl.Popup()
    //   .setHTML('Hello');

    // const marker = new mapboxgl.Marker()
    //   .setLngLat([25.4833039, 42.7249925])
    //   .setPopup(popup)
    //   .addTo(this.map);

    // this.map.on('click', e => {
    //   marker.setLngLat(e.lngLat.wrap());
    // });
    //===========================================

    //===========================================
    // Polygons
    // https://docs.mapbox.com/mapbox-gl-js/example/mapbox-gl-draw/?fbclid=IwAR2WskyTrU1GsuYcEo25ov0KGtgwHU6MBURfTPSVPd04RXNyKaHPUyccbCg
    //===========================================
    const draw = new MapboxDraw({
      displayControlsDefault: false,
      controls: {
        polygon: true,
        trash: true
      }
    });
    this.map.on('draw.create', () => {
      this.updateArea(draw);
    });
    this.map.on('draw.delete', () => {
      this.updateArea(draw);
    });
    this.map.on('draw.update', () => {
      this.updateArea(draw);
    });

    //===========================================

    // Controls
    this.map.addControl(new mapboxgl.FullscreenControl());
    this.map.addControl(new mapboxgl.NavigationControl());
    this.map.addControl(new mapboxgl.GeolocateControl({
      positionOptions: {
        enableHighAccuracy: true
      },
      trackUserLocation: true
    }));
    this.map.addControl(draw);

    this.map.on('load', () => {
      // Draw farms
      this.map.addLayer({
        'id': 'firstFarm',
        'type': 'fill',
        'source': {
          'type': 'geojson',
          'data': { "type": "FeatureCollection", "features": [{ "id": "a3b6118db3dc7191914583d9f9d252b2", "type": "Feature", "properties": {}, "geometry": { "coordinates": [[[26.78194983328177, 43.56830092622141], [26.825491541712893, 43.59888543636157], [26.7529220276613, 43.60461830215871], [26.78194983328177, 43.56830092622141]]], "type": "Polygon" } }, { "id": "e74b1a1cf218f2c3b1c59e4e07605158", "type": "Feature", "properties": {}, "geometry": { "coordinates": [[[26.668477502218423, 43.63900402481704], [26.692227524998998, 43.611305955147316], [26.766116484761312, 43.64664262568789], [26.718616439200048, 43.67050699342391], [26.668477502218423, 43.63900402481704]]], "type": "Polygon" } }, { "id": "fdf3d258c0634e68693b294c79f26600", "type": "Feature", "properties": {}, "geometry": { "coordinates": [[[26.38611612027313, 43.61035062166968], [26.438893948674178, 43.57212483972236], [26.34389385755165, 43.530048438698515], [26.292435474860326, 43.55013402167404], [26.38611612027313, 43.61035062166968]]], "type": "Polygon" } }] }
        },
        'layout': {},
        'paint': {
          'fill-color': '#00ff00',
          'fill-opacity': 0.3
        }
      });

      // Draw marker
      new mapboxgl.Marker()
        .setLngLat([26.810106065329023, 43.54925891313479])
        .setPopup(new mapboxgl.Popup().setHTML("Your bee-hive"))
        .addTo(this.map);
    });

    this.map.on('click', 'firstFarm', (e) => {
      const coords = ((e.features[0].geometry) as any).coordinates[0];
      const bounds = new mapboxgl.LngLatBounds();

      for (let i = 0; i < coords.length; i++) {
        bounds.extend(coords[i]);
      }

      // displays always top coordinates
      const lnglat = bounds.getCenter();

      const description = 'Description';

      new mapboxgl.Popup()
        .setLngLat(lnglat)
        .setHTML(description)
        .addTo(this.map);
    });
  }

  updateArea(draw: any) {
    const data = draw.getAll();

    console.log(JSON.stringify(data)); // GeoJSON object

    if (data.features.length > 0) {

      let area = turf.area(data);
      console.log(`area = ${area}`);
      // restrict to area to 2 decimal points
      let rounded_area = Math.round(area * 100) / 100;
      console.log(`rounded_area = ${rounded_area}`);

    }
  }
}
