import { Component, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-information-modal',
  template: `
  <div class="modal-header">
    <h4 class="modal-title pull-left">{{ title }}</h4>
    <button type="button" class="close pull-right" aria-label="Close" (click)="bsModalRef.hide()">
        <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p>{{ message }}</p>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-primary" (click)="bsModalRef.hide()">Ok</button>
  </div>
  `
})
export class InformationModalComponent {
  title: string;
  message: string;

  constructor(public bsModalRef: BsModalRef) { }
}
