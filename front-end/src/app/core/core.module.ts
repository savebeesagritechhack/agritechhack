import { NgModule, Optional, SkipSelf } from '@angular/core';

import { HttpErrorHandlerService } from './services/http-error-handler.service';
import { ValuesService } from './services/values.services';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';

import { AuthGuard } from './guards/auth.guard';
import { NoSessionGuard } from './guards/no-session.guard';

const services = [
  HttpErrorHandlerService,
  ValuesService,
  AuthService,
  UserService
];

const guards = [
  AuthGuard,
  NoSessionGuard
];

@NgModule({
  imports: [],
  declarations: [],
  exports: [],
  providers: [...services, ...guards]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule has already been loaded. You should only import Core modules in the AppModule only.');
    }
  }
}
