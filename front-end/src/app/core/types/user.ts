export interface User {
  id: number;
  firstName: string;
  middleName: string;
  lastName: string;
  email: string;
  phone: string;
  roles: string[];
}

export interface AccessToken {
  accessToken: string;
  refreshToken: string;
  expiration: Date;
}