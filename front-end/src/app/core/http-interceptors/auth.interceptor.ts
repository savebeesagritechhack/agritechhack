import { Injectable, ErrorHandler } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse
} from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { catchError, switchMap, take, filter } from 'rxjs/operators';

import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private refreshTokenInProgress = false;
  private tokenSubject = new BehaviorSubject<any>(null);

  constructor(
    private router: Router,
    private authService: AuthService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {

          if (error instanceof HttpErrorResponse) {
            if (error.status === 401) {
              return this.handleHttpResponseError(request, next);
            }
          }

          return throwError(error);
        })
      );
  }

  private handleHttpResponseError(request: HttpRequest<any>, next: HttpHandler) {
    // Get access token from local storage
    const accessToken = this.authService.getJwtToken();

    if (!this.refreshTokenInProgress) {
      // if token is not set, let the request execute
      if (!accessToken) {
        return next.handle(request);
      }

      // set authorization header if the token haven't expired yet
      if (!this.authService.isTokenExpired()) {
        return next.handle(this.attachTokenToRequest(request, accessToken));
      }

      this.refreshTokenInProgress = true;
      this.tokenSubject.next(null);

      return this.authService.getNewAccessToken()
        .pipe(
          switchMap(tokens => {
            console.log('tokens');

            this.refreshTokenInProgress = false;
            this.tokenSubject.next(tokens);

            return next.handle(this.attachTokenToRequest(request, accessToken));
          }),
          catchError(error => {

            // TODO: This is executed at some point because there is no "Authorization: Bearer token" in headers
            console.log('catched you');

            this.refreshTokenInProgress = false;

            // TODO: Not working
            this.authService.logout();

            this.router.navigate(['/log-in']);

            return throwError(error);
          })
        );
    } else {
      // if token refreshing is in progress - wait for new token
      this.refreshTokenInProgress = false;

      return this.tokenSubject.pipe(filter(token => token != null),
        take(1),
        switchMap(() => {
          return next.handle(this.attachTokenToRequest(request, accessToken));
        }));
    }
  }

  private attachTokenToRequest(request: HttpRequest<any>, token: string) {
    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`
      }
    });
  }
}
