import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

import { User } from '../types/user';

@Injectable()
export class UserService {
  private actionUrl: string;

  constructor(private httpClient: HttpClient) {
    this.actionUrl = `${environment.baseUrls.server}api/users`;
  }

  register(user: User) {
    return this.httpClient.post<User>(this.actionUrl, user);
  }
}
