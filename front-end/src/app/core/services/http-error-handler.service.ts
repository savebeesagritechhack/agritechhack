import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';

import { InformationModalComponent } from 'src/app/shared/information-modal/information-modal.component';

@Injectable()
export class HttpErrorHandlerService {
  constructor(private modalService: BsModalService) { }

  handle(error: Error | HttpErrorResponse) {
    if (error instanceof HttpErrorResponse) {
      // Server or connection error happened
      switch (error.status) {
        case -1:
          this.openInformationModal('You appear to be offline. Please check your internet connection and try again.');
          break;
        case 0:
          this.openInformationModal('Server is offline.');
          break;
        case 400:
          this.openInformationModal(`${error.error}`);
          break;
        case 403:
          this.openInformationModal(`You don't have the required permissions`);
          break;
        case 404:
          this.openInformationModal('Resource not found');
          break;
        case 422:
          this.openInformationModal('Invalid data provided');
          break;
        case 500:
        case 501:
        case 502:
        case 503:
          this.openInformationModal('An internal server error occurred');
          break;
      }
    } else {
      // Handle Client Error (Angular Error, ReferenceError...)
      console.log('Handle Client Error (Angular Error, ReferenceError...)');
    }
  }

  private openInformationModal(message: string) {
    const initialState = {
      title: 'Information',
      message: message
    };

    this.modalService.show(InformationModalComponent, { initialState });
  }
}
