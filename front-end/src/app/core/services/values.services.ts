import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class ValuesService {
  private actionUrl: string;

  constructor(private httpClient: HttpClient) {
    this.actionUrl = `${environment.baseUrls.server}api/values`;
  }

  getAllValues() {
    return this.httpClient.get<string[]>(this.actionUrl);
  }

  getValueById(id: number) {
    return this.httpClient.get<string>(`${this.actionUrl}/${id}`);
  }

  addValue(value: string) {
    return this.httpClient.post<string>(this.actionUrl, value);
  }

  updateValue(value: string) {
    return this.httpClient.put<string>(`${this.actionUrl}/${value}`, value);
  }

  deleteValue(id: number) {
    return this.httpClient.delete(`${this.actionUrl}/${id}`);
  }
}
