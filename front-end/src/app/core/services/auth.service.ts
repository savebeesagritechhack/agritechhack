import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

import { AccessToken } from '../types/user';

@Injectable()
export class AuthService {
  private readonly JWT_TOKEN = 'access_token';
  private readonly REFRESH_TOKEN = 'refresh_token';
  private actionUrl: string;

  constructor(private httpClient: HttpClient) {
    this.actionUrl = `${environment.baseUrls.server}api/auth`;
  }

  isLoggedIn(): boolean {
    return !!this.getJwtToken();
  }

  login(credentials: { email: string, password: string }) {
    return this.httpClient.post<AccessToken>(`${this.actionUrl}/login`, credentials)
      .pipe(
        map(tokens => {
          localStorage.setItem(this.JWT_TOKEN, tokens.accessToken);
          localStorage.setItem(this.REFRESH_TOKEN, tokens.refreshToken);
          return true;
        })
      );
  }

  getNewAccessToken() {
    const helper = new JwtHelperService();
    const email = helper.decodeToken(this.getJwtToken()).sub;

    return this.httpClient.post<AccessToken>(`${this.actionUrl}/token/refresh`, { 'refreshToken': localStorage.getItem(this.REFRESH_TOKEN), 'email': email })
      .pipe(
        map(tokens => {
          console.log(`JWT: ${this.getJwtToken()}`);

          localStorage.setItem(this.JWT_TOKEN, tokens.accessToken);
          localStorage.setItem(this.REFRESH_TOKEN, tokens.refreshToken);
          return true;
        })
      );
  }

  getJwtToken(): string {
    return localStorage.getItem(this.JWT_TOKEN);
  }

  logout() {
    return this.httpClient.post<AccessToken>(`${this.actionUrl}/token/revoke`, { 'refreshToken': localStorage.getItem(this.REFRESH_TOKEN) })
      .pipe(
        map(tokens => {
          console.log('logged out');

          localStorage.removeItem(this.JWT_TOKEN);
          localStorage.removeItem(this.REFRESH_TOKEN);
          return true;
        })
      );
  }

  isTokenExpired(): boolean {
    const helper = new JwtHelperService();

    // const decodedToken = helper.decodeToken(this.getJwtToken());
    // console.log(decodedToken);

    return helper.isTokenExpired(this.getJwtToken());
  }
}
