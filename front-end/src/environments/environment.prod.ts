export const environment = {
  production: true,
  baseUrls: {
    server: 'http://localhost:5000/'
  },
  mapbox: {
    accessToken: 'pk.eyJ1IjoiaHVsa3N0YW5jZSIsImEiOiJjazBpYThlbmYwYWIzM2ttejJtOTEwZW0xIn0.H8FN0uQsc4oxiByim452ow'
  }
};
